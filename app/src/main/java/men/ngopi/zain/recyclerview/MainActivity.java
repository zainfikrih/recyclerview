package men.ngopi.zain.recyclerview;

import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private ArrayList<OperatingSystem> list = new ArrayList<OperatingSystem>();;
    private CustomAdapter adapter;
    private RecyclerView recyclerView;
    private SwipeRefreshLayout swipeRefreshLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        swipeRefreshLayout = findViewById(R.id.swiperefresh);

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                refreshData();
            }
        });
        recyclerView = findViewById(R.id.myRecyclerview);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(MainActivity.this));

        addData();

        adapter = new CustomAdapter(getApplicationContext(), list);
        adapter.notifyDataSetChanged();
        recyclerView.setAdapter(adapter);
    }

    private void refreshData(){
        addData();
        adapter.notifyDataSetChanged();
        swipeRefreshLayout.setRefreshing(false);
        Toast.makeText(this, "Data Refreshed",Toast.LENGTH_SHORT).show();
    }

    private void addData(){
        list.clear();
        list.add(new OperatingSystem(R.drawable.android, "Android"));
        list.add(new OperatingSystem(R.drawable.ios, "iPhone"));
        list.add(new OperatingSystem(R.drawable.windows_mobile, "Windows Mobile"));
        list.add(new OperatingSystem(R.drawable.blackberry, "Blackberry"));
        list.add(new OperatingSystem(R.drawable.webos, "Web OS"));
        list.add(new OperatingSystem(R.drawable.ubuntu, "Ubuntu"));
        list.add(new OperatingSystem(R.drawable.windows, "Windows 7"));
        list.add(new OperatingSystem(R.drawable.mac, "Mac OS"));
    }
}
