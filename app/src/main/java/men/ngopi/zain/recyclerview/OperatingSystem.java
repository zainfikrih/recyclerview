package men.ngopi.zain.recyclerview;

import android.content.res.Resources;

public class OperatingSystem {
    String title;
    int image;

    public OperatingSystem(int image, String title) {
        this.image = image;
        this.title = title;
    }

    public int getImage() {
        return image;
    }

    public String getTitle() {
        return title;
    }

    public void setImage(int image) {
        this.image = image;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
